#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

import jinja2
import json
import os
import webapp2

DEFAULT_HEADERS = {
    'Cache-Control': 'private',
    'Expires': 'Sun, 11 Mar 1984 12:00:00 GMT',
    'Max-Age': '0',
}


JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True)


class MainHandler(webapp2.RequestHandler):
    def get(self):
        self.response.headers.update(DEFAULT_HEADERS)
        self.response.headers['Content-Type'] = 'text/html'
        # self.response.write("%s\n" % json.dumps({'ip': self.request.remote_addr}))
        template_values = {
            'ip': self.request.remote_addr,
        }
        template = JINJA_ENVIRONMENT.get_template('index.html')
        self.response.write(template.render(template_values))


class JSONHandler(webapp2.RequestHandler):
    def get(self):
        self.response.headers.update(DEFAULT_HEADERS)
        self.response.headers['Content-Type'] = 'application/json'
        self.response.write("%s\n" % json.dumps({'ip': self.request.remote_addr}))


class TextHandler(webapp2.RequestHandler):
    def get(self):
        self.response.headers.update(DEFAULT_HEADERS)
        self.response.headers['Content-Type'] = 'text/plain'
        self.response.write("%s\n" % self.request.remote_addr)


class XMLHandler(webapp2.RequestHandler):
    def get(self):
        self.response.headers.update(DEFAULT_HEADERS)
        self.response.headers['Content-Type'] = 'text/xml'
        # self.response.write("%s\n" % json.dumps({'ip': self.request.remote_addr}))
        template_values = {
            'ip': self.request.remote_addr,
        }
        template = JINJA_ENVIRONMENT.get_template('index.xml')
        self.response.write(template.render(template_values))


app = webapp2.WSGIApplication([
    ('/', MainHandler),
    ('/json/?', JSONHandler),
    ('/text/?', TextHandler),
    ('/xml/?', XMLHandler),
], debug=True)
